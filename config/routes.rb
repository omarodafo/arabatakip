Rails.application.routes.draw do
  devise_for :users
  resources :posts, path: 'gonderi'

  scope(path_names: { new: 'yeni', edit: 'duzenle' }) do
    resources :categories, path: 'kategori'
  end
  
  root to: 'posts#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
