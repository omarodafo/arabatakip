require 'test_helper'

class AractakipsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @aractakip = aractakips(:one)
  end

  test "should get index" do
    get aractakips_url
    assert_response :success
  end

  test "should get new" do
    get new_aractakip_url
    assert_response :success
  end

  test "should create aractakip" do
    assert_difference('Aractakip.count') do
      post aractakips_url, params: { aractakip: { plaka: @aractakip.plaka, sorun: @aractakip.sorun } }
    end

    assert_redirected_to aractakip_url(Aractakip.last)
  end

  test "should show aractakip" do
    get aractakip_url(@aractakip)
    assert_response :success
  end

  test "should get edit" do
    get edit_aractakip_url(@aractakip)
    assert_response :success
  end

  test "should update aractakip" do
    patch aractakip_url(@aractakip), params: { aractakip: { plaka: @aractakip.plaka, sorun: @aractakip.sorun } }
    assert_redirected_to aractakip_url(@aractakip)
  end

  test "should destroy aractakip" do
    assert_difference('Aractakip.count', -1) do
      delete aractakip_url(@aractakip)
    end

    assert_redirected_to aractakips_url
  end
end
