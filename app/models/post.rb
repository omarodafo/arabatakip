class Post < ApplicationRecord
    belongs_to :category
    belongs_to :user

    def word_count
        self.body.split.size
    end

    def reading_time
        (word_count / 180.0).ceil
    end

end