class PostsController < ApplicationController
    def index
        @posts = Post.all
    end
    
    def show
        @post = Post.find(params[:id])
    end
    
    def new
        @post = Post.new
    end
    
    def create
        @post = current_user.posts.build(post_params)
        if @post.save
            redirect_to posts_path
        else
            flash[:danger] = "Bir hata oluştu"
            render 'new'
        end
    end
    
    def edit
        @post = Post.find(params[:id])
    end
    
    def update
        @post = Post.find(params[:id])
        if @post.update_attributes(post_params)
            flash[:success] = "#{@post.title} Güncellendi."
            redirect_to post_path
        else
            render 'edit'
            flash[:danger] = "Başarısız"
        end
    end
    
    def destroy
        @post = Post.find(params[:id])
        if @post.destroy
            flash[:success] = "Silindi!"
            redirect_to posts_path
        else
            flash[:danger] = "Başarısız"
        end
    end

    private
        def post_params
            params.require(:post).permit(:title, :body, :category_id)
        end
end
