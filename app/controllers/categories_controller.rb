class CategoriesController < ApplicationController
    def index
        @categories = Category.all
    end
    
    def show
        @category = Category.find(params[:id])
    end
    
    def new
        @category = Category.new
    end
    
    def create
        @category = current_user.categories.build(category_params)
        if @category.save
            redirect_to new_post_path
        else
            render plain: "hata"
            render 'new'
        end
    end
    
    def destroy
        @category = Category.find(params[:id])
        if @category.destroy
            flash[:success] = "Başarıyla Silindi"
            redirect_to categories_path
        else
            flash[:danger] = "Başarısız"
        end
    end
    
    private
    
    def category_params
        params.require(:category).permit(:title, :user_id)
    end
    
end